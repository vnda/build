import storeInfos from "./utils/storeInfos.js";
import clearDist from "./utils/clearDist.js";
import processDocumentation from "./utils/processDocumentation.js";
import processFiles from "./utils/processFiles.js";
import copyAssets from "./utils/copyAssets.js";
import uploadFiles from "./utils/uploadFiles.js";
import warningMessage from "./utils/warningMessage.js";
import checkTokens from "./utils/checkTokens.js";
import processDependencies from "./utils/processFiles/processDependencies.js";

const storePath = storeInfos.path;

const sync = async () => {

  const tokenStatus = await checkTokens('staging', storeInfos.store)

  if (!tokenStatus.ok) {
    return warningMessage(tokenStatus.message)
  }

  await clearDist(storePath);
  await processDependencies(storeInfos)
  await processDocumentation(storePath);
  await processFiles(storePath);
  await copyAssets(storePath);
  await uploadFiles(storeInfos);

  return warningMessage('Sync finalizado!');
};

sync();