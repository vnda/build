import processScss from "../utils/processFiles/processScss.js";
import processLiquids from "../utils/processFiles/processLiquids.js";
import processJs from "../utils/processFiles/processJs.js";

export default async function processFiles(storePath) {
    await processScss(storePath);
    await processLiquids(storePath);
    await processJs(storePath);
    return;
}