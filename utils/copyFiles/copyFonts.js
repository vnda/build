import fse from 'fs-extra';
import path from 'path';

const copyFonts = async function(projectPath) {
    const fontsEntryPoints = `${projectPath}/assets/fonts`;
    const outputPath = path.resolve(projectPath, 'dist', 'public', 'fonts');

    if (!fse.existsSync(fontsEntryPoints)) {
        return;
    }

    await fse.copy(fontsEntryPoints, outputPath);
}

export default copyFonts;