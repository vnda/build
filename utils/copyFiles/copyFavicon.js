import fse from 'fs-extra';
import path from 'path';

const copyFavicon = async function(projectPath) {
    const faviconEntryPoints = `${projectPath}/assets/favicon.ico`;
    const outputPath = path.resolve(projectPath, 'dist', 'public', 'favicon.ico');

    if (!fse.existsSync(faviconEntryPoints)) {
        return;
    }

    await fse.copy(faviconEntryPoints, outputPath);
}

export default copyFavicon;