import fse from 'fs-extra';
import path from 'path';

const copyImages = async function(projectPath) {
    const imagesEntryPoints = `${projectPath}/assets/images`;
    const outputPath = path.resolve(projectPath, 'dist', 'public', 'images');

    if (!fse.existsSync(imagesEntryPoints)) {
        return;
    }

    await fse.copy(imagesEntryPoints, outputPath);
}

export default copyImages;