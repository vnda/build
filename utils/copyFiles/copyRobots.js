import fse from 'fs-extra';
import path from 'path';

const copyRobots = async function(projectPath) {
    const robotsEntryPoints = `${projectPath}/assets/robots.txt`;
    const outputPath = path.resolve(projectPath, 'dist', 'public', 'robots.txt');

    if (!fse.existsSync(robotsEntryPoints)) {
        return;
    }

    await fse.copy(robotsEntryPoints, outputPath);
}

export default copyRobots;