import fse from 'fs-extra';
import path from 'path';

const copySiteMap = async function(projectPath) {
    const siteMapEntryPoints = `${projectPath}/assets/sitemap.xml`;
    const outputPath = path.resolve(projectPath, 'dist', 'public', 'sitemap.xml');

    if (!fse.existsSync(siteMapEntryPoints)) {
        return;
    }

    await fse.copy(siteMapEntryPoints, outputPath);
}

export default copySiteMap;