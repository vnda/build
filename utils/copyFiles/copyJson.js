import fse from 'fs-extra';
import path from 'path';
import { glob } from 'glob';

const copyJson = async function(projectPath) {
    const jsonEntryPoints = [
        `${projectPath}/assets/*.json`
    ];
    const paths = await glob(jsonEntryPoints, {absolute: true});
    const outputPath = path.resolve(projectPath, 'dist', 'public');

    paths.map(async(file) => {
        const fileName = path.basename(file, path.extname(file));
        await fse.copy(file, path.resolve(outputPath, fileName));
    });
}

export default copyJson;