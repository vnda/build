import fse from 'fs-extra';
import path from 'path';
import { glob } from 'glob';

const copyMinifyJs = async function(projectPath) {
    const jsEntryPoints = [
        `${projectPath}/assets/javascripts/**/*.min.js`
    ];
    const paths = await glob(jsEntryPoints, {absolute: true});
    const outputPath = path.resolve(projectPath, 'dist', 'public', 'javascripts');

    paths.map(async(file) => {
        const fileName = path.basename(file, path.extname(file));
        await fse.copy(file, path.resolve(outputPath, fileName));
    });
}

export default copyMinifyJs;