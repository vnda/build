import fs from 'fs'
import path from 'path'

// Lida com o arquivo dependencies.js
export default async function processDependencies(storeInfos) {

  const dependenciesPath = path.resolve(storeInfos.path, 'dependencies.js').replace(/\\/g, '/');

  try {
    // Verifica se o arquivo existe
    fs.accessSync(dependenciesPath);

    const dependencies = await import(`file://${dependenciesPath}`);
    const modules = dependencies.default.modules;

    modules.forEach(module => {
      let moduleName = Object.keys(module).toString();
      console.log('Node module:', '\x1b[35m', moduleName, '\x1b[0m');

      // Para os arquivos da dependência
      module[moduleName]['files'].forEach(filePath => {
        //console.log('Module file:', filePath);
        let pathDestiny = '';
        let extension = path.extname(filePath);

        switch (extension) {
          case '.js':
            pathDestiny = 'javascripts';
            break;
          case '.css':
            pathDestiny = 'stylesheets';
            break;
          case '.gif':
          case '.svg':
          case '.png':
          case '.jpg':
            pathDestiny = 'images';
            break;
          case '.ttf':
          case '.woff':
          case '.woff2':
          case '.otf':
          case '.eot':
            pathDestiny = 'fonts';
            break;
        }

        const destPath = path.resolve(storeInfos.path, `dist/public/${pathDestiny}`);
        const destFilePath = path.join(destPath, path.basename(filePath)).replace(/\\/g, '/');

        try {
          fs.mkdirSync(destPath, { recursive: true });
          fs.copyFileSync(filePath, destFilePath);
        } catch (copyError) {
          console.error(`Erro ao copiar o arquivo ${filePath}:`, copyError);
        }

      });

    });

  } catch (error) {
    if (error.code === 'ENOENT') {
      console.error('Arquivo dependencies.js não encontrado:', dependenciesPath);
    } else {
      console.error('Erro ao processar dependencies.js:', error);
    }
  }
}