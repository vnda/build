import { glob } from 'glob';
import path from 'path';
import fs from 'fs/promises';
import webpack from 'webpack';
import TerserPlugin from 'terser-webpack-plugin';

const processjs = async function (projectPath) {
    const jsEntryPoints = [
        path.join(projectPath, 'assets', 'javascripts', '!(*.min).js').replace(/\\/g, '/'),
        path.join(projectPath, 'assets', 'javascripts', 'pages', '!(*.min).js').replace(/\\/g, '/'),
    ];
    const paths = await glob(jsEntryPoints, { absolute: true, cwd: path.resolve(projectPath) });
    const outputPath = path.resolve(projectPath, 'dist', 'public', 'javascripts');

    await fs.mkdir(outputPath, { recursive: true });

    const compilePromises = paths.map((file) => {
        const fileName = path.basename(file, path.extname(file));

        const compiler = webpack({
            mode: 'production',
            devtool: 'source-map',
            stats: 'minimal',
            entry: file,
            cache: false,
            output: {
                path: outputPath,
                filename: `${fileName}.js`
            },
            optimization: {
                minimize: false,
                minimizer: [
                    new TerserPlugin({
                        minify: TerserPlugin.swcMinify,
                        parallel: true,
                        extractComments: false,
                        terserOptions: {
                            ecma: 5,
                            safari10: false,
                            mangle: true,
                            compress: true,
                            format: {
                                comments: false,
                            },
                        },
                    }),
                ],
            },
        });

        return new Promise((resolve, reject) => {
            compiler.run((err, stats) => {
                if (err) {
                    console.error('Erro durante o processamento:', err);
                    reject(err);
                } else if (stats.hasErrors()) {
                    const info = stats.toJson();
                    console.error("\n", info.errors[0].message, "\n\n");
                } else {
                    resolve(stats);
                }
            });
        });
    });

    try {
        await Promise.all(compilePromises);
    } catch (err) {
        console.error('Erro ao processar os arquivos js', err.toString());
    }
}

export default processjs;