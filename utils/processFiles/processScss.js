import path from 'path';
import * as sass from 'sass';
import postcss from 'postcss';
import cssnano from 'cssnano';
import autoprefixer from 'autoprefixer';
import { glob } from 'glob';
import { mkdirp } from 'mkdirp';
import { writeFile } from 'fs/promises';

const processScss = async function (storePath) {
  const stylesheetsPath = {
    src: path.join(storePath, 'assets', 'sass', '**', '!(custom_style).scss').replace(/\\/g, '/'),
    dist: path.resolve(storePath, 'dist', 'public', 'stylesheets')
  };

  try {
    // Obter todos os arquivos SCSS
    const files = await glob(stylesheetsPath.src);
    const scssFiles = files.filter(file => !path.basename(file).startsWith('_'));

    // Compila cada um dos arquivos
    const compilePromises = scssFiles.map(async (file) => {
      const outputName = file.split('sass')[1].replace('.scss', '.css');
      const outputPath = path.join(stylesheetsPath.dist, outputName).replace(/\\/g, '/');
      const compiled = sass.compile(file);

      // Garante que o diretório existe no dist
      await mkdirp(path.join(stylesheetsPath.dist, path.dirname(outputName)).replace(/\\/g, '/'))

      const result = await postcss([autoprefixer(), cssnano()])
        .process(compiled.css, { from: file });

      await writeFile(outputPath, result.css);
    });
    await Promise.all(compilePromises);
  } catch (err) {
    console.error('\nError ao processar os arquivos Scss', "\n\n");
    console.log(err)
  }
}

export default processScss;