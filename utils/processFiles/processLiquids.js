import fs from 'fs/promises';
import path from 'path';
import { glob } from 'glob';
import { inlineSource } from 'inline-source';

const processLiquids = async function (projectPath) {
    const cssPath = path.join(projectPath, 'dist', 'public', 'stylesheets').replace(/\\/g, '/')
    const destDir = path.join(projectPath, 'dist').replace(/\\/g, '/')
    const liquidsPath = path.join(projectPath, 'liquids', '**', '*.liquid').replace(/\\/g, '/')
    const files = await glob(liquidsPath, { absolute: true, cwd: path.resolve(projectPath) });

    const liquidOptions = {
        rootpath: cssPath,
        ignore: ['script'],
    };

    try {
        const compilePromises = files.map(async (file) => {
            const filePath = path.resolve(file);
            let fileContent = await fs.readFile(filePath, 'utf8');

            fileContent = await inlineSource(fileContent, liquidOptions);

            const splitPath = filePath.split('liquids');
            const destPath = path.join(destDir, splitPath[1]);
            await fs.mkdir(path.dirname(destPath), { recursive: true });
            await fs.writeFile(destPath, fileContent, 'utf8');
        });

        await Promise.all(compilePromises);
    } catch (err) {
        console.error('Erro ao processar os arquivos inlineCss', err.toString());
    }
}

export default processLiquids;