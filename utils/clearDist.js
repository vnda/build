import fse from 'fs-extra';

export default async function clearDist(storePath){
    const distPath = `${storePath}/dist`;
    fse.removeSync(distPath);
    return;
}