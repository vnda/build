import 'dotenv/config'

export default async function (mode, shopCode) {

  let result = { ok: false, message: 'CheckTokens - Não foi possível verificar o Token. Estado inicial não foi alterado.' }

  if (mode !== 'staging' && mode !== 'production') {
    result.message = 'CheckTokens - Modo não definido'
  }

  const tokenType = mode === 'staging' ? 'STGTOKEN' : 'PRODTOKEN'
  const tokenValue = process.env[tokenType];

  const checks = {
    undefined: `${tokenType} não encontrado.\nVerifique se você a variável ${tokenType} está definida no arquivo .env da build.\n\nVerifique também se você está com os scripts na versão mais atual.`,
    'undefined': `Token ${tokenType} não econtrado!\n\nVocê criou o arquivo .env e definiou os tokens dentro do arquivo?`,
    '': `Token ${tokenType} não definido corretamente!\n\nVerifique a variável ${tokenType} no arquivo .env da build.`,
    ' ': `Token ${tokenType} não definido corretamente!\n\nVerifique a variável ${tokenType} no arquivo .env da build.`,
    '  ': `Token ${tokenType} não definido corretamente!\n\nVerifique a variável ${tokenType} no arquivo .env da build.`
  };


  // Verifica se o token foi setado corretamente nos arquivos
  if (checks.hasOwnProperty(tokenValue)) {
    result.ok = false
    result.message = checks[tokenValue];
    return result
  }

  // Verifica se o token pode operar na loja
  let url = 'https://api.sandbox.vnda.com.br/api/v2/templates/upsert';
  if (mode == 'production') {
    url = 'https://api.vnda.com.br/api/v2/templates/upsert'
  }

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      "accept": "application/json",
      "authorization": `Bearer ${tokenValue}`,
      "content-type": "application/json",
      "X-Shop-Code": shopCode
    },
    body: JSON.stringify({ path: 'test.liquid', body: 'test' })
  });

  if (response.status === 401) {
    result.ok = false
    result.message = `Token não autorizado!\nToken: ${tokenValue.substring(0, 4)}***`

  } else {
    result.ok = true
    result.message = 'Tudo ok com o Token'
  }

  return result
}