import uploadLiquids from '../utils/uploadFiles/uploadLiquids.js';
import uploadCSS from '../utils/uploadFiles/uploadCSS.js';
import uploadJs from '../utils/uploadFiles/uploadJs.js';
import uploadDependencies from '../utils/uploadFiles/uploadDependencies.js';

export default async function uploadFiles(storeInfos, deploy_prod, base_rules) {
  let storePath = storeInfos.path;
  const codev1 = storeInfos.codev1;
  let storeName = storeInfos.store;
  const token = process.env.STGTOKEN;

  // Garante funcionamento do codev1 para deploy
  if (deploy_prod) {
    if (codev1 != '' && codev1 != 'undefined') {
      storeName = codev1
    }
  }

  // Recupera regras de upload da chamada do script
  let rules = storeInfos.rules || [];
  if (typeof rules == 'string') {
    rules = rules.split(',')
  }

  if (Array.isArray(base_rules)) {
    base_rules.forEach(rule => {
      rules.push(rule)
    })
  }

  await uploadLiquids(storePath, storeName, token, rules, deploy_prod);
  await uploadCSS(storePath, storeName, token, rules, deploy_prod);
  await uploadJs(storePath, storeName, token, rules, deploy_prod);
  await uploadDependencies(storePath, storeName, token, rules, deploy_prod)
  return;
}
