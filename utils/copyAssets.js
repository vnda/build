import copyMinifyJs from './copyFiles/copyMinifyJs.js';
import copyImages from './copyFiles/copyImages.js';
import copyFonts from './copyFiles/copyFonts.js';
import copyJson from './copyFiles/copyJson.js';
import copyFavicon from './copyFiles/copyFavicon.js';
import copyRobots from './copyFiles/copyRobots.js';
import copySiteMap from './copyFiles/copySiteMap.js';

export default async function copyAssets(storePath){

    await Promise.all([
        copyMinifyJs(storePath),
        copyImages(storePath),
        copyFonts(storePath),
        copyJson(storePath),
        copyFavicon(storePath),
        copyRobots(storePath),
        copySiteMap(storePath)
    ]);
}