import concatDocs from './processDocumentation/concatDocs.js';
import syncDocIndex from './processDocumentation/syncDocIndex.js';
import generateTable from './processDocumentation/generateTable.js';

export default async function processDocumentation(storePath) {
    await generateTable(storePath);
    await concatDocs(storePath);
    await syncDocIndex(storePath);
}