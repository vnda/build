import yargs from 'yargs';
import { dirname } from 'path'

let storeInfos = yargs(process.argv).argv;

storeInfos.path = dirname(storeInfos._[1].replace(/\\/g, '/'));
storeInfos.path = storeInfos.path.replace('/build', `/${storeInfos.store}`)

export default storeInfos;