const allowUpload = (file, rules) => {
  if (!file || !rules) return true

  // Valida se o arquivo está dentro das regras para upload
  const fileType = `.${file.replace('.map', '').split('.').pop()}`
  const filename = file.split('/').pop().replace('.map', '')
  const uploadRules = rules || []
  let allowUpload = false

  if (file.includes('noMinifiedFolder') || file.includes('index.md') || file.includes('/prints') || file.includes('_above.css')) return false

  // Verifica se o arquivo deve subir, com base nas regras definidas
  if (uploadRules == 'undefined') allowUpload = true

  if (Array.isArray(uploadRules)) {
    if (uploadRules.includes(fileType) || uploadRules.length === 0) {
      allowUpload = true
    }

    uploadRules.forEach(rule => {
      // no_logs é usado quando é necessário outros uploads por debaixo dos panos
      // arquivos nesta regra devem subir. Usado pra atualizar css_above, enviando liquids por debaixo dos panos
      if (rule == 'no_logs') {
        allowUpload = true
      }
      // Verifica se a regra é referente a arquivo e se o arquivo bate com o nome da regra
      if (rule.includes('.') && filename.includes(rule)) {
        allowUpload = true
      }

      // verifica arquivo conforme o tipo
      if (fileType.includes(rule)) {
        allowUpload = true
      }

    })
  }

  return allowUpload
}

export default allowUpload