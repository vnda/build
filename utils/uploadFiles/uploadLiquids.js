import fs from 'fs/promises';
import { glob } from 'glob';
import fetch from 'node-fetch';
import storeInfos from '../storeInfos.js';
import allowUpload from './allowUpload.js';
import path from 'path';

const uploadLiquids = async function (storePath, storeName, token, rules, deploy_prod) {
  const preview = storeInfos.preview
  const liquidEntryPoints = path.join(storePath, 'dist', '**', '*.liquid').replace(/\\/g, '/');
  const paths = await glob(liquidEntryPoints, { absolute: true, cwd: path.resolve(storePath) });
  let shopCode = storeName;

  // Controller para abortar envio em caso de statuscode negativo na requisição
  const globalController = new AbortController();
  const globalSignal = globalController.signal;

  const uploadPromises = paths.map(async (file) => {

    if (globalSignal.aborted) {
      return Promise.reject(new Error("Operação abortada"));
    }

    // Verifica se arquivo está liberado para upload dentro das rules
    const allowed = allowUpload(file, rules)
    if (!allowed) return

    const data = await fs.readFile(file, { encoding: 'utf-8' });
    let path = file.replace(/\\/g, '/').split('dist/')[1];

    if (preview !== undefined && preview !== 'undefined' && preview !== '') {
      if (preview != '' && preview != ' ') {
        path = `previews/${preview}/${path}`
      }
    }

    const file_data = {
      path: path,
      body: data
    };

    let url = 'https://api.sandbox.vnda.com.br/api/v2/templates/upsert';

    if (deploy_prod && deploy_prod === true) {
      url = 'https://api.vnda.com.br/api/v2/templates/upsert'
      token = process.env.PRODTOKEN
    }

    // Controller local, para não conflitar com o global. Cuida da operação somente deste fetch
    const localController = new AbortController()
    const localSignal = localController.signal

    const timeoutId = setTimeout(() => {
      localController.abort();
      console.log(`\nTimeout: abortando upload do arquivo ${path}`);
    }, 10000);

    try {
      const response = await fetch(url, {
        signal: localSignal,
        method: 'POST',
        headers: {
          "accept": "application/json",
          "authorization": `Bearer ${token}`,
          "content-type": "application/json",
          "X-Shop-Code": shopCode
        },
        body: JSON.stringify(file_data)
      });

      if (!globalSignal.aborted) {
        if (Array.isArray(rules)) {
          if (!rules.includes('no_logs')) {
            console.log(`${deploy_prod ? 'PROD' : 'STG'} ${storeInfos.store} - Arquivo atualizado com sucesso! ${path}`)
          }
        } else {
          console.log(`${deploy_prod ? 'PROD' : 'STG'} ${storeInfos.store} - Arquivo atualizado com sucesso! ${path}`)
        }
      }

    } catch (error) {
      console.log(`\nERRO na atualização do arquivo: ${file}`);
      console.log(error)
    } finally {
      clearTimeout(timeoutId);
    }
  });

  try {
    await Promise.all(uploadPromises);
  } catch (error) {
    if (!globalSignal.aborted) {
      console.error('\nErro durante o upload liquid:', error);
    } else {
      console.error('\nErro no globalSignal do upload liquid:', error)
    }
  }
}

export default uploadLiquids;