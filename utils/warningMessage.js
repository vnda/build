export default function warningMessage(string) {

  if (typeof string == 'string') {
    console.log('\n======================================================\n')
    console.log(string)
    console.log('\n======================================================\n')
  } else {
    console.log('Erro na mensagem de aviso.')
  }
}
