import fs from 'fs';
import { exec } from 'child_process';
import path from 'path';

export default async function generateTable(storePath) {
	const generateFile = path.join(storePath, 'generateTable.js').replace(/\\/g, '/')
	if (fs.existsSync(generateFile)) {
		return exec(`node ${generateFile}`, function (err, stdout, stderr) {
			console.log(stdout);
			console.log(stderr);
		});
	}
}