import fs from 'fs';
import { glob } from 'glob';
import path from 'path';

export default async function concatDocs(storePath) {
    const pathsDir = path.join(storePath, 'docs', '*.md').replace(/\\/g, '/')
    const distPath = path.join(storePath, 'dist', 'public', 'docs', 'index.md').replace(/\\/g, '/')

    const srcPaths = await glob(pathsDir);

    srcPaths.sort();

    let concatenatedContent = '';

    for (let i = 0; i < srcPaths.length; i++) {
        const srcPath = srcPaths[i];

        const content = fs.readFileSync(srcPath, 'utf8');
        concatenatedContent += content + '\n';
    }

    fs.mkdirSync(path.dirname(distPath), { recursive: true });
    fs.writeFileSync(distPath, concatenatedContent, 'utf8');
}