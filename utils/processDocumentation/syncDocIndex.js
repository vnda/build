import fs from 'fs';
import path from 'path';
import { spawn } from 'child_process';

export default async function syncDocIndex(storePath) {
    const marpFile = path.join(storePath, '.marprc.json').replace(/\\/g, '/')
    if (fs.existsSync(marpFile)) {
        const cmd = process.platform === 'win32' ? 'npx.cmd' : 'npx';
        const distPath = path.join(storePath, 'dist', 'public', 'docs').replace(/\\/g, '/');

        var marp = spawn(cmd, ['marp', '-c', `${storePath}/.marprc.json`, `${storePath}/dist/public/docs/index.md`], { stdio: 'inherit' });
        return marp.on('close', function (code) {
            const replaceInFile = (filePath, searchValue, replaceValue) => {
                const data = fs.readFileSync(filePath, 'utf8');
                const result = data.replace(searchValue, replaceValue);
                fs.writeFileSync(filePath, result, 'utf8');
            };

            const copyFile = (src, dest) => {
                fs.copyFileSync(src, dest);
            };

            const indexPath = path.resolve(`${storePath}/dist/public/docs/index.html`);
            replaceInFile(indexPath, '<body', '<body style="background:#f9f8f8;"');
            replaceInFile(indexPath, '/body>', 'script src="../docs/documentation.js"></script></body>');

            copyFile(path.join(storePath, 'docs', 'documentation.css').replace(/\\/g, '/'), path.resolve(distPath, 'documentation.css'));
            copyFile(path.join(storePath, 'docs', 'documentation.js').replace(/\\/g, '/'), path.resolve(distPath, 'documentation.js'));
        });
    }
}