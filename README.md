# Build

Para saber mais sobre o nosso ambiente de desenvolvimento, acesse nosso [Portal de desenvolvedor](https://developers.vnda.com.br/docs/ambiente-de-desenvolvimento)

&nbsp;

## Scripts previstos na Build

Os scripts previstos na build já estão configurados no package.json de cada loja. Caso precise atualizar uma loja antiga, seguir os passos dentro da pasta sample_scripts. Garanta que você tenha um token de desenvolvedor configurado e ativo na loja, do contrário os scripts não irão funcionar.

&nbsp;

| script           | descrição                                                                                             |
| ---------------- | ----------------------------------------------------------------------------------------------------- |
| `npm run dev`    | Inicia o gulp watch para escutar as alterações feitas nos arquivos, compilar e enviar ao servidor     |
| `npm run sync`   | Refaz a pasta dist, recompilando todos os arquivos, e envia para o servidor no bucket de **staging**  |
| `npm run deploy` | Refaz a pasta dist, recompilando todos os arquivos, e envia para o servidor no bucket de **produção** |
| `npm run clearpreview` | _Não adicionado por padrão nas lojas_, precisa ser adicionado manualmente, seguindo o padrão dos demais. Remove os arquivos do preview especificado, mas também possui alguns comandos adicionais (ver abaixo) |
| `npm run doc` | _Não adicionado por padrão nas lojas_, precisa ser adicionado manualmente, seguindo o padrão dos demais. Recompila e sincroniza os arquivos de documentação da loja |

&nbsp;

## Upload condicional de arquivos

Você pode limitar a quantidade de arquivos que o script de `deploy` envia para o servidor, através da flag _--rules_, adicionada na chamada do script. Você pode passar uma lista de **nomes de arquivos**, ou uma lista dos **tipos** que deseja subir, e o script executará o upload somente dos arquivos que condizem com a regra.

**Exemplo de definição de regras no arquivo .env**:
```
npm run deploy -- --rules=js,layout.liquid,home.css
```

***Tipos de arquivos que você pode passar na lista***
- `js`
- `css`
- `liquid`

&nbsp;

## Lojas v2

Adicionar no package.json da loja, dentro das configs, o parâmetro _codev1_. O valor dele deve ser o shop code do v1 da loja.

Isto é necessário porque o novo ambiente de v2 é aberto somente em _staging_ (e, por consequência, é gerado um novo shop code para o v2), mas em _production_ continua valendo o shop code antigo. Este novo parâmetro no package.json define qual é o shop code original do v1, para que o deploy seja feito para o code correto.

Exemplo:
```
"name": "bolovo2",
"repository": "gitlab:vnda/setup/bolovo2",
version": "3.0.0",
"config": {
  "liquid": "4.0.3",
  "url": "https://www.bolovo.com.br/",
  "debug": "true",
  "codev1": "bolovo"
},
```

&nbsp;

## Previews

Os previews são um recurso de staging que permite ter múltiplas visualizações do mesmo store, onde cada uma delas utilizará os arquivos e assets específicos do preview definido.

&nbsp;

### Definindo um preview

Na chamada dos scripts `dev` e `sync`, adicione a flag _--preview_ junto da chamada, contendo o nome do preview desejado:
```
npm run dev -- --preview=nome-preview
```
Se o preview é novo, execute primeiro o script de `sync` para popular o ambiente com os arquivos. Depois, a programação segue normalmente com o script `dev`

&nbsp;

### Utilizando o script clearpreview

Este script permite apagar os arquivos de um preview no servidor, para remover código desnecessário uma vez que o script foi encerrado.

Ao rodar este script, ele irá **apagar todos os arquivos do preview** que estiver definido no arquivo .env. _Precisa passar a flag --preview junto na execução do script_ para que esse script funcione corretamente.

&nbsp;

Além do nome do preview, também tem a opção de usar 2 keywords de auxílio no processo:

```
npm run clearpreview -- --preview=list-all
```
Ao rodar o comando com este valor na variável, exibirá uma lista de todos os previews encontrados neste store.

&nbsp;

```
npm run clearpreview -- --preview=clear-all
```
Ao rodar o comando com este valor na variável, apagará os arquivos de **todos os previews exitentes** neste store.

