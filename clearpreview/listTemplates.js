import fetch from 'node-fetch';

const listTemplates = async function (store) {

  try {

    const response = await fetch('https://api.sandbox.vnda.com.br/api/v2/templates', {
      headers: {
        "accept": "application/json",
        "authorization": `Bearer ${process.env.STGTOKEN}`,
        "content-type": "application/json",
        "X-Shop-Code": store
      },
    })

    return await response.json()

  } catch (error) {
    console.log('Erro ao buscar lista de todos os templates')
    return console.log(error)
  }
}

export default listTemplates