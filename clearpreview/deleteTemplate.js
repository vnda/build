import fetch from 'node-fetch';

const deleteTemplate = async function (file_path, store) {
  try {
    const response = await fetch(`https://api.sandbox.vnda.com.br/api/v2/templates/${file_path}`, {
      method: 'DELETE',
      headers: {
        "accept": "application/json",
        "authorization": `Bearer ${process.env.STGTOKEN}`,
        "content-type": "application/json",
        "X-Shop-Code": store
      },
    })

    if (response.status !== 200 && response.status !== 204) {
      return `Erro ao apagar: ${file_path} - Status Code ${response.status}`
    }

    return (`Remoção finalizada: ${file_path}`)

  } catch (error) {
    console.log(error)
    return `Erro na requisição para apagar: ${file_path}`
  }
}

export default deleteTemplate