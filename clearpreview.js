import checkTokens from "./utils/checkTokens.js"
import warningMessage from "./utils/warningMessage.js"
import storeInfos from "./utils/storeInfos.js"
import listTemplates from "./clearpreview/listTemplates.js"
import listAssets from "./clearpreview/listAssets.js"
import deleteTemplate from "./clearpreview/deleteTemplate.js"
import deleteAsset from "./clearpreview/deleteAsset.js"

const clearPreview = async function () {
  let preview = storeInfos.preview
  const tokenStatus = await checkTokens('staging', storeInfos.store)

  if (!tokenStatus.ok) {
    return warningMessage(tokenStatus.message)
  }

  // Captura somente os arquivos dentro de previews
  const templates = await listTemplates(storeInfos.store)
  const assets = await listAssets(storeInfos.store)

  let previewFiles = []
  templates.forEach(file => { if (file.path.includes('previews/')) previewFiles.push(file) })
  assets.forEach(file => { if (file.path.includes('previews/')) previewFiles.push(file) })

  if (preview === 'list-all') {
    // Lista todos os previews
    let previews = []
    previewFiles.forEach(file => {
      const previewName = file.path.split('/')[1]
      if (!previews.includes(previewName)) previews.push(previewName)
    })

    let string = 'Previews encontrados:\n\n'
    previews.forEach(preview => { string += `${preview}\n` })
    return warningMessage(string)

  }

  preview = (preview.toLowerCase() === 'clear-all') ? '' : `${preview.toLowerCase()}`

  // Apaga todos os arquivos do preview listado, ou de todos os previews
  for (let index = 0; index < templates.length; index++) {
    const template = templates[index];
    if (template.path.includes(`previews/${preview}`)) {
      const response = await deleteTemplate(template.path, storeInfos.store)
      console.log(response)
    }
  }

  for (let index = 0; index < assets.length; index++) {
    const asset = assets[index];
    if (asset.path.includes(`previews/${preview}`)) {
      const response = await deleteAsset(asset.path, storeInfos.store)
      console.log(response)
    }
  }

  return warningMessage('clearpreview finalizado!')

}

clearPreview()