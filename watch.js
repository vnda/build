import storeInfos from "./utils/storeInfos.js";
import checkTokens from './utils/checkTokens.js';

import chokidar from 'chokidar';
import warningMessage from './utils/warningMessage.js';

import processLiquids from "./utils/processFiles/processLiquids.js";
import uploadLiquids from './utils/uploadFiles/uploadLiquids.js';

import processScss from "./utils/processFiles/processScss.js";
import uploadCSS from './utils/uploadFiles/uploadCSS.js';

import processJs from "./utils/processFiles/processJs.js";
import uploadJs from './utils/uploadFiles/uploadJs.js';

import copyAssets from './utils/copyAssets.js';
import uploadDependencies from './utils/uploadFiles/uploadDependencies.js';

import path from 'path';

const watch = async () => {
  const storeHost = storeInfos.store;
  const storePath = storeInfos.path;
  const STGTOKEN = process.env.STGTOKEN
  const storePreview = storeInfos.preview;

  // Bloqueia caso problema com Token
  const tokenStatus = await checkTokens('staging', storeInfos.store)
  if (!tokenStatus.ok) {
    return warningMessage(tokenStatus.message)
  }

  // Exibe primeira mensagem pro user
  console.clear();

  const preview = storePreview || '';

  let text = ` Sincronizando o staging:\n https://${preview !== '' ? `${preview}--` : ''}${storeHost}.vnda.dev`;
  if (preview !== '') text = ` >> Preview em uso: ${preview}\n\n` + text;
  warningMessage(text);

  // Seta watcher e inicia
  const watcher = chokidar.watch(storePath, {
    ignored: (path) => path.includes('dist/') || path.includes('node_modules/') || path.includes('.git') || path.includes('/scripts/'),
    awaitWriteFinish: {
      stabilityThreshold: 300,
      pollInterval: 100,
    },
  });

  watcher.on('change', async (file) => {
    const fileType = file.split('.').pop();
    const fileName = path.basename(file);

    if (fileType === 'liquid') {
      await processLiquids(storePath);
      uploadLiquids(storePath, storeHost, STGTOKEN, [fileName]);
      return
    }

    if (fileType === 'scss') {
      let upload_inline = false
      let rules = [fileName.replace('.scss', '.css')]

      // libera upload de liquid pros arquivos inline
      if (fileName.includes('_above') || fileName.includes('main.scss')) {
        upload_inline = true
        rules = [fileName]
      }

      // se salvar um componente de css, sobe todos os arquivos indiscriminadamente
      if (fileName.split('')[0] == '_') {
        rules = []
        upload_inline = true
      }

      await processScss(storePath);
      uploadCSS(storePath, storeHost, STGTOKEN, rules, false);

      if (upload_inline) {
        await processLiquids(storePath);
        uploadLiquids(storePath, storeHost, STGTOKEN, ['no_logs']);
        console.log(`STG ${storeHost} - Arquivo atualizado com sucesso! CSS Inline: ${file.split(storeHost).pop()}`)
      }

      return
    }

    if (fileType === 'js') {
      await processJs(storePath);
      const jsRules = []

      // Garante o upload somente do arquivo que foi salvo
      // Verifica se o arquivo salvo está dentro das pastas principais, para limitar somente a ele
      const globalFolders = ['/javascripts/', '/javascripts/pages/']
      for (const folder of globalFolders) {
        if (file.includes(`${folder}${fileName}`)) {
          jsRules.push(fileName)
          break
        }
      }

      // Verifica se é componente direto de uma página, para subir somente o arquivo daquela página
      if (file.includes('/pages/')) {
        const origin = file.split('/pages/')[1].split('/')[0]
        jsRules.push(`${origin}.js`)
      }

      uploadJs(storePath, storeHost, STGTOKEN, jsRules);
      return
    }

    if (fileType === 'png' || fileType === 'jpg' || fileType === 'jpeg' || fileType === 'gif' || fileType === 'svg' || fileType === 'ico' || fileType === 'xml' || fileType === 'json') {
      await copyAssets(storePath);
      uploadDependencies(storePath, storeHost, STGTOKEN, [fileName])
      return
    }
  });
}

watch()