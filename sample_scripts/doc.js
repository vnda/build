const execSync = require('child_process').execSync
const package = require('../package.json');

const lineArgs = process.argv.filter(arg => {
  if (arg.includes('--') && arg.includes('=')) return arg
})

execSync(`cd ../build && node doc.js --store=${package.name} --liquid=${package.config.liquid} --debug=${package.config.debug} ${lineArgs.join(' ')}`, { stdio: [0, 1, 2] })
