const execSync = require('child_process').execSync
const package = require('../package.json');

const lineArgs = process.argv.filter(arg => {
  if (arg.includes('--') && arg.includes('=')) return arg
})

execSync(`cd ../build && node clearpreview.js --store=${package.name} --liquid=${package.config.liquid} --debug=${package.config.debug} ${lineArgs.join(' ')}`, { stdio: [0, 1, 2] })

//npm run clearpreview -- --preview=list-all -> lista todos previews existentes
//npm run clearpreview -- --preview=clear-all -> apaga todos os previews existentes
//npm run clearpreview -- --preview=nome-do-preview -> apaga os arquivos do preview em questão

