const execSync = require('child_process').execSync
const package = require('../package.json');

const lineArgs = process.argv.filter(arg => {
  if (arg.includes('--') && arg.includes('=')) return arg
})

execSync(`cd ../build && node deploy.js --store=${package.name} --liquid=${package.config.liquid} --codev1=${package.config.codev1} ${lineArgs.join(' ')}`, { stdio: [0, 1, 2] })
