const execSync = require('child_process').execSync
const package = require('../package.json');

const lineArgs = process.argv.filter(arg => {
  if (arg.includes('--') && arg.includes('=')) return arg
})

execSync(`cd ../build && node watch.js --store=${package.name} ${lineArgs.join(' ')}`, { stdio: [0, 1, 2] })
