1. No repositório da loja, crie uma pasta com o nome "scripts" na raiz do projeto

2. Copie os três scripts da pasta sample_scripts para dentro dessa pasta criada:
- deploy.js
- dev.js
- sync.js

4. No arquivo package.json da loja, substitua os scripts para os que estão dentro do arquivo scripts.json

6. Crie um arquivo ".env" na pasta da build. Dentro dele, coloque as informações com base no exemplo do arquivo ".env.sample"
