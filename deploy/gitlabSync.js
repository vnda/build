import { resolve } from 'path';
import storeInfos from '../utils/storeInfos.js';
import { execSync } from 'child_process';
import warningMessage from '../utils/warningMessage.js';
import simpleGit, { CleanOptions } from 'simple-git';

const outputFolder = resolve(`../${storeInfos.store}/`);
const AUTO_REF_NAME = 'auto-deploy'

simpleGit().clean(CleanOptions.FORCE);
const git = simpleGit(outputFolder, { binary: 'git' })

const gitlabSync = {
	handleCommitPush: async function () {
		console.log(`\n>> Iniciando o envio dos arquivos para o gitlab...`)

		// garante que usa o remote correto
		let repoPath = `git@gitlab.com:vnda/setup/${storeInfos.store}.git`
		if (process.env.AGENT !== 'vnda') {
			repoPath = `git@gitlab.com:vnda/ag-ncias/${process.env.AGENT.toLowerCase()}/${storeInfos.store}.git`
		}

		console.log(`\n>> Remote origin: ${repoPath}`)

		// Commita os arquivos alterados
		const status = await git.status()
		if (!status.isClean()) {
			console.log(`\n>> Realizando o commit dos arquivos pendentes...`)
			const timestamp = Date.now()
			const date = new Date(timestamp)
			await git.add(['.'])
			await git.commit(`Deploy ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`)
		}

		// Garante que a branch atual não é a branch de auto-deploy
		const branchList = await git.branchLocal()
		const defaultEntry = branchList.all.filter(branch => {
			if (branch == 'main' || branch == 'master') return branch
		})[0]

		const currentBranch = branchList.current == AUTO_REF_NAME ? defaultEntry : branchList.current

		await git.checkout(currentBranch)

		// Recria a branch de auto-deploy
		for (const branch of branchList.all) {
			if (branch === AUTO_REF_NAME) {
				await git.deleteLocalBranch(AUTO_REF_NAME, true)
			}
		}

		await git.checkoutLocalBranch(AUTO_REF_NAME)

		// Adiciona o remote local, caso não tenha
		const remotes = await git.getRemotes()
		let hasRemote = false
		remotes.forEach(remote => {
			if (remote.name === AUTO_REF_NAME) hasRemote = true
		})

		if (!hasRemote) {
			console.log(`\n>> Remote para ${AUTO_REF_NAME} não encontrado. Criando remote...\n`)
			await git.addRemote(AUTO_REF_NAME, repoPath)
		}

		// Busca por atualizações gerais em todos os remotes, através de git fetch
		const remotesForFetch = await git.branch({ '-r': true })
		let haveToFetch = true
		remotesForFetch.all.forEach(remote => {
			if (remote.includes(`${AUTO_REF_NAME}/main`) || remote.includes(`${AUTO_REF_NAME}/master`)) haveToFetch = false
		})

		if (haveToFetch) {
			try {
				console.log(`\n>> Buscando informações do remote ${AUTO_REF_NAME}. Por favor, autentique caso necessário...\n`)
				execSync(`cd ../${storeInfos.store} && git fetch ${AUTO_REF_NAME}`, { stdio: [0, 1, 2] })

			} catch (error) {
				console.log(`\n>> Repositório remoto para ${AUTO_REF_NAME} não encontrado. Caso você tenha autenticado corretamente e o gitlab apresentou erro "fatal: could not read from remote repository", aguarde o processo de criação, que será executado numa etapa adiante. Se você não autenticou corretamente, a etapa de criação futura poderá gerar erro.\n`)
			}
		}

		// Verifica se já existe branch main e cria, caso não tenha
		const remoteBranches = await git.branch({ '-r': true })

		console.log('\n>> Branches remotos encontrados:', remoteBranches.all)

		if (!remoteBranches.all.includes(`${AUTO_REF_NAME}/main`) && !remoteBranches.all.includes(`${AUTO_REF_NAME}/master`)) {
			try {
				console.log(`\n>> Repositório para ${AUTO_REF_NAME} não encontrado. Criando o repositório, por favor autentique caso necessário...`)
				execSync(`cd ../${storeInfos.store} && git push ${AUTO_REF_NAME} main`, { stdio: [0, 1, 2] })
			} catch (error) {
				console.log(`\n>> Erro ao criar o repositório para ${AUTO_REF_NAME} no gitlab.`)
				console.log(error.message);
				await git.checkout(currentBranch, {})
				return false
			}
		}

		// Faz o push
		try {
			console.log(`\n>> Enviando o código para a branch de ${AUTO_REF_NAME}. Por favor, autentique caso necessário...`)
			execSync(`cd ../${storeInfos.store} && git push --force ${AUTO_REF_NAME} ${AUTO_REF_NAME}`, { stdio: [0, 1, 2] })
			await git.checkout(currentBranch, {})
			console.log('\n>> Envio dos arquivos para o gitlab finalizado com sucesso.')
			return true
		} catch (error) {
			console.log('\n>> Erro no envio dos arquivos para o gitlab.')
			console.log(error.message);

			await git.checkout(currentBranch, {})
			return false
		}
	},

	init: async function () {
		const isRepo = await git.checkIsRepo()

		if (!isRepo) {
			warningMessage(` Repositório git não encontrado!\n\n Verifique a importação dos arquivos do template base\n ou inicie o projeto git com o comando:\n\n git init --initial-branch main`)
			return false
		}

		const done = gitlabSync.handleCommitPush()
		return done
	}
}

export default gitlabSync
