import storeInfos from "./utils/storeInfos.js";
import checkTokens from './utils/checkTokens.js';
import warningMessage from './utils/warningMessage.js';

import clearDist from "./utils/clearDist.js";
import processFiles from "./utils/processFiles.js";
import processDependencies from "./utils/processFiles/processDependencies.js";
import copyAssets from "./utils/copyAssets.js";
import uploadFiles from "./utils/uploadFiles.js";
import gitlabSync from "./deploy/gitlabSync.js";

const storePath = storeInfos.path;

const deploy = async () => {
    let storeName = storeInfos.store
    const codev1 = storeInfos.codev1;
    if (codev1 != '' && codev1 != 'undefined') {
        storeName = codev1
    }
    const tokenStatus = await checkTokens('production', storeName)

    if (!tokenStatus.ok) {
        return warningMessage(tokenStatus.message)
    }

    if (process.env.AGENT == 'undefined') {
        return warningMessage(` Agente não definido!\n\n Coloque corretamente a variável AGENT no arquivo .env`)
    }

    if (process.env.AGENT !== 'vnda') {
        const gitStatus = await gitlabSync.init()
        if (!gitStatus)
            return warningMessage(' Erro no upload dos arquivos para o Gitlab!\n\n Verifique os logs anteriores para identificar o problema.')
    }

    await clearDist(storePath);
    await processDependencies(storeInfos)
    await processFiles(storePath);
    await copyAssets(storePath);
    await uploadFiles(storeInfos, true);

    return warningMessage('Deploy finalizado!');
};

deploy();