import storeInfos from "./utils/storeInfos.js";
import processDocumentation from "./utils/processDocumentation.js";
import uploadFiles from "./utils/uploadFiles.js";
import warningMessage from "./utils/warningMessage.js";
import checkTokens from "./utils/checkTokens.js";

const storePath = storeInfos.path;

const sync = async () => {

  const tokenStatus = await checkTokens('staging', storeInfos.store)

  if (!tokenStatus.ok) {
    return warningMessage(tokenStatus.message)
  }

  await processDocumentation(storePath);

  setTimeout(async () => {
    await uploadFiles(storeInfos, false, ['index.html', 'documentation.js', 'documentation.css']);
    return warningMessage('Upload da documentação finalizado!');
  }, 3000)

};

sync();